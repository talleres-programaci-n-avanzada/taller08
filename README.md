# Taller08

## Números Amigos (1.5) 
Se  dice  que  dos  números  son  amigos  si  cada  uno  de  ellos  es  igual  a  la  suma  de  los divisores del otro. 
```text 
    Ejemplo: 
    284 1 + 2 + 4 + 71 + 142 da 220 
    220 1 + 2 + 4 + 5 + 10 + 11 + 20 + 44 + 55 + 110 da 284 
```

Utilice sólo funciones recursivas para resolver elproblema. 

## Escribir Vertical (1.5) 
Diseñe  una  función  recursiva  quedado  un  número  lo  escriba  colocando  los  dígitos verticalmente. 
```text 
    Ejemplo:
    1984 se debe ver: 
    1 
    9 
    8 
    4 
```
    
## Elección (2.0) 
La  fórmula  para  calcular  el  número  de  formas  de  elegir  r  cosas  distintas  de  un conjunto de n cosas es: 
```text 
C(n,r) = n! / (r! * (n –r)!)
```

La función factorial n! se define como 
```text 
N! = n * (n –1) * (n –2) * ... *1 
```

Elabore una versión recursiva de la fórmula anterior que calcule la fórmula.