#include <iostream>

using namespace std;

int calculo(int n, int r);

int main() {
    int n, r;
    cout << "Ingrese n (número de elementos del conjunto): ";
    cin >> n;
    cout << "Ingrese r (número de elementos a elegir): ";
    cin >> r;

    if (n < r) {
        cout << "Error: n debe ser mayor o igual que r." << "\n";
    } else {
        int resultado = calculo(n, r);
        cout << "C(" << n << ", " << r << ") = " << resultado << "\n";
    }

    return 0;
}

int calculo(int n, int r){
    if (r == 0 || r == n) {
        return 1;
    }
    return calculo(n - 1, r - 1) + calculo(n - 1, r);
}