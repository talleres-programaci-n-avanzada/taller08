#include <iostream>

using namespace std;

int suma(int num, int divisor = 1, int sum = 0);

int main() {
    int num1, num2, suma1, suma2;
    cout << "Ingrese el primer número: ";
    cin >> num1;
    cout << "Ingrese el segundo número: ";
    cin >> num2;
    suma1 = suma(num1);
    suma2 = suma(num2);

    if (suma1 == num2 && suma2 == num1) {
        cout << "Los números son amigos.\n";
        cout << "Número 1: " << num1 << ", Suma 2: " << suma2 << "\n";
        cout << "Número 2: " << num2 << ", Suma 1: " << suma1 << "\n";
    } else {
        cout << "Los números no son amigos.\n";
        cout << "Número 1: " << num1 << ", Suma 2: " << suma2 << "\n";
        cout << "Número 2: " << num2 << ", Suma 1: " << suma1 << "\n";
    }

    return 0;
}

int suma(int num, int divisor, int sum) {
    if (divisor > num / 2) {
        return sum;
    }
    if (num % divisor == 0) {
        sum += divisor;
    }
    return suma(num, divisor + 1, sum);
}
