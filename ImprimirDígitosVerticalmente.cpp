#include <iostream>

using namespace std;

void imprimirDigitos(int num);

int main() {
    int num;
    cout << "Ingrese un número: ";
    cin >> num;

    cout << "Dígitos del número verticalmente:" << endl;
    imprimirDigitos(num);

    return 0;
}

void imprimirDigitos(int num){
    if (num < 10) {
        cout << num << "\n";
        return;
    }

    imprimirDigitos(num / 10);
    cout << num % 10 << "\n";
}